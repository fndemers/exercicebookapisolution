#!/bin/bash



#run unittest
python3 -m unittest discover -s project -v -p test_*.py
if [ $? -ne 0 ]
then
	echo "unittest failed, aborting"
	exit 1
fi


#Stop container
docker stop paa

#Clean containers (better version than prune)
docker rm paa

#Rebuild image (removing it before for better cleanup)
docker rmi python-add-api
docker build -t python-add-api -f ./project/docker/Dockerfile .

#start container
docker run -p 5555:5556 -d --name paa python-add-api

#run deploiement tests
python3 -m unittest discover -s project -v -p dtest_*.py
