import sys
from flask import *

app = Flask("book api")

books = [
	{'id':0,
	'title':'Dawnthief',
	'author':'James Barclay',
	'series':'The chronicles of the raven',
	'year_published':2003,
	'rating':'veryGood'},
	{'id':1,
	'title':'Magician Apprentice',
	'author':'Raymond Feist',
	'series':'The Riftwar Saga',
	'year_published':1982,
	'rating':'veryGood'},
	{'id':2,
	'title':'Firedrake',
	'author':'Richard Knaak',
	'series':'Legends of the Dragonream',
	'year_published':2000,
	'rating':'veryGood'},
	{'id':3,
	'title':'Thelma la licorne',
	'author':'Aaron Blabey',
	'series':'N/A',
	'year_published':2017,
	'rating':'amazing'}
]

@app.route("/")
def hello():
	return '''<h1>Library</h1>
			<p>A simple API with books</p>'''

@app.route("/books/all")
def getAll():
	return jsonify(books)

def getSpecificBooks(paramName, paramValue):
	results = []
	for book in books:
		if book[paramName] == paramValue:
			results.append(book)
	return results

@app.route("/books/one")
def getOne():
	results = []
	if 'id' in request.args:
		try:
			id = int(request.args['id'])
		except:
			abort(400, "The argument provided for the id field needs to be an integer")
		results = getSpecificBooks('id', id)
	elif 'rating' in request.args:
		rating = request.args['rating']
		results = getSpecificBooks('rating', rating)
	else:
		abort(400, "One field needs to be specified to search for single books. Use either id or rating")

	return jsonify(results)

def getAllAuthors():
	authors = set()
	for book in books:
		authors.add(book['author'])
	authorList = list(authors)
	authorList.sort()
	return authorList

@app.route("/authors/all")
def getAllAuthorsRoute():
	return jsonify(getAllAuthors())


if __name__ == '__main__':
	#simple version:
#	app.run(debug = True, host="0.0.0.0", port=givenPort)
	#More detailed version
	if len(sys.argv) == 2:
		try:
			givenPort = int(sys.argv[1])
			if givenPort > 0 and givenPort < 65536:
				app.run(debug = True, host="0.0.0.0", port=givenPort)
			else:
				print("Launching BookAPI requires exactly 1 argument which is the port (an integer between 1 and 65535). The argument provided (" + sys.argv[0] + ") was not in that range." )
				exit(1)
		except ValueError as ve:
			print("Launching BookAPI requires exactly 1 argument which is the port (an integer). The argument provided (" + sys.argv[0] + ") was not an int." )
			exit(1)
	else:
		print("Launching BookAPI requires exactly 1 argument which is the port. " + str(len(sys.argv)-1) + " arguments were provided.")
		exit(1)
