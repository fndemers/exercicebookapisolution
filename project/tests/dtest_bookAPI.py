import unittest
import requests
import json

from codeAPI import bookAPI


IP = "127.0.0.1"
PORT = "5555"
URL = "http://" + IP + ":" + PORT + "/"

class BasicTests(unittest.TestCase):

	def test_home(self):
		response = requests.get(URL)
		self.assertEqual(200, response.status_code)
		self.assertIn('Library', response.content.decode('utf-8'))

	def test_BooksAll(self):
		response = requests.get(URL + "books/all")
		self.assertEqual(200, response.status_code)
		self.assertCountEqual(bookAPI.books, json.loads(response.content.decode('utf-8')))

	def test_AuthorsAll(self):
		response = requests.get(URL + "authors/all")
		self.assertEqual(200, response.status_code)
		self.assertCountEqual(['James Barclay','Raymond Feist', 'Richard Knaak', 'Aaron Blabey'] , json.loads(response.content.decode('utf-8')))

	def test_BooksOne(self):
		response = requests.get(URL + "books/one?id=0")
		self.assertEqual(200, response.status_code)
		obj = json.loads(response.content.decode('utf-8'))
		self.assertEqual(1,len(obj))
		self.assertCountEqual(bookAPI.books[0], obj[0])


if __name__ == '__main__':
	unittest.main()
