import unittest

from codeAPI import bookAPI

class BasicTests(unittest.TestCase):

#	def test_nothing(self):
#		self.assertTrue(1==34, "I don't always test my code; but when I do, I do it in PRODUCTION.")

	def test_getAllAuthors(self):
		actual = bookAPI.getAllAuthors()
		expected = ['Aaron Blabey', 'James Barclay', 'Raymond Feist', 'Richard Knaak']
		self.assertCountEqual(expected, actual)

	def test_getSpecificBooks(self):
		actual = bookAPI.getSpecificBooks('id', 0)
		expected = [{'id':0,'title':'Dawnthief','author':'James Barclay','series':'The chronicles of the raven','year_published':2003,'rating':'veryGood'}]
		self.assertCountEqual(expected, actual)


	def test_getSpecificBooks2(self):
		actual = bookAPI.getSpecificBooks('id', 4)
		expected = []
		self.assertCountEqual(expected, actual)


	def test_getSpecificBooks3(self):
		actual = bookAPI.getSpecificBooks('rating', 'veryGood')
		self.assertEqual(3, len(actual))
		#This is a weak test.

	def test_getSpecificBooks4(self):
		actual = bookAPI.getSpecificBooks('rating', 'amazing')
		expected = [{'id':3,'title':'Thelma la licorne','author':'Aaron Blabey','series':'N/A','year_published':2017,'rating':'amazing'}]
		self.assertCountEqual(expected, actual)


if __name__ == '__main__':
	unittest.main()
